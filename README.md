## Trading View Single Ticker Widget for phpBB ##

The widget allows to show Trading View Single Ticker Widget on search page results.

![Search Results](screenshot-1.png)

See https://www.tradingview.com/widget/single-ticker/

**NOTE:** The new custom template event **search_results_postbody_after** should be added to **search_results.html** file manually:

```sh
--- phpBB/styles/prosilver/template/search_results.html 2018-06-07 14:55:18.799612429 +0300
+++ phpBB/styles/prosilver/template/search_results.html 2018-06-07 14:54:26.816098276 +0300
@@ -194,6 +194,7 @@
                <div class="postbody">
                        <h3><a href="{searchresults.U_VIEW_POST}">{searchresults.POST_SUBJECT}</a></h3>
                        <div class="content">{searchresults.MESSAGE}</div>
+                       <!-- EVENT search_results_postbody_after -->
                </div>
        <!-- ENDIF -->
```
