<?php
/**
 *
 * This file is part of the phpBB Forum Software package.
 *
 * @copyright (c) phpBB Limited <https://www.phpbb.com>
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 * For full copyright and license information, please see
 * the docs/CREDITS.txt file.
 *
 */

namespace athc\singletickerwidget\event;

/**
 * @ignore
 */
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener
 */
class widget_listener implements EventSubscriberInterface
{
	static public function getSubscribedEvents()
	{
		return array(
			'core.user_setup' => array(array('load_language_on_setup'), array('define_constants')),
			'core.search_modify_tpl_ary' => 'search_modify_tpl_ary',
		);
	}

	/* @var \phpbb\controller\helper */
	protected $helper;

	/* @var \phpbb\template\template */
	protected $template;

	/* @var \phpbb\db\driver\driver_interface */
	protected $db;

	/**
	 * Constructor
	 *
	 * @param \phpbb\controller\helper $helper   Controller helper object
	 * @param \phpbb\template\template $template Template object
	 */
	public function __construct(\phpbb\controller\helper $helper, \phpbb\template\template $template, \phpbb\db\driver\driver_interface $db)
	{
		$this->helper = $helper;
		$this->template = $template;
		$this->db = $db;
	}

	public function define_constants()
	{
		include_once __DIR__ . '/../includes/constants.php';
	}

	/**
	 * Load the Acme Demo language file
	 *     acme/demo/language/en/demo.php
	 *
	 * @param \phpbb\event\data $event The event object
	 */
	public function load_language_on_setup($event)
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = array(
			'ext_name' => 'athc/singletickerwidget',
			'lang_set' => 'singletickerwidget',
		);
		$event['lang_set_ext'] = $lang_set_ext;
	}

	/**
	 * Set Symbol ID assigned to topic ID
	 */
	public function search_modify_tpl_ary($event)
	{
        if (isset($event['row'])) {
            $array = $event['tpl_ary'];
			$sql = 'SELECT `symbol`, `exchange`
            FROM ' . SYMBOLS_TOPICS_TABLE . ' st
            LEFT JOIN ' . SYMBOLS_TABLE . ' s ON s.symbol_id=st.symbol_id
            WHERE topic_id = "' . $this->db->sql_escape($event['row']['topic_id']) . '"';
			$result = $this->db->sql_query($sql);
			$row = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);
            if ($row['symbol']) {
                $array['EXCHANGE'] = $row['exchange'];
                $array['SYMBOL'] = $row['symbol'];
            }
            $event['tpl_ary'] = $array;
        }
    }

}
