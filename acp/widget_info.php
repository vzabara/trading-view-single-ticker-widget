<?php

namespace athc\singletickerwidget\acp;

class widget_info
{
	public function module()
	{
		return array(
			'filename'  => '\athc\singletickerwidget\acp\widget_module',
			'title'     => 'ACP_SINGLETICKER_TITLE',
			'modes'    => array(
				'settings'  => array(
					'title' => 'ACP_SINGLETICKER_SETTINGS',
					'auth'  => 'ext_athc/singletickerwidget && acl_a_board',
					'cat'   => array('ACP_SINGLETICKER_TITLE'),
				),
			),
		);
	}
}