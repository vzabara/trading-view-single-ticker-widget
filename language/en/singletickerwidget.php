<?php

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'SINGLETICKER_PAGE'              => 'Single Ticker Widget',
	'ACP_SINGLETICKER_TITLE'         => 'Single Ticker Widget',
	'ACP_SINGLETICKER_EXPLAIN'       => '',
	'ACP_SINGLETICKER_WIDGETS'       => 'Single Ticker Widgets',
	'ACP_SINGLETICKER_SETTINGS'      => 'Single Ticker Widget',
	'ACP_SINGLETICKER_SAVED'         => 'Record have been saved successfully!',
	'EDIT_WIDGET'                  => 'Edit Widget',
	'ADD_WIDGET'                   => 'Add Widget',
	'NO_WIDGET'                    => 'Single Ticker widget was not found',
	'ACP_NO_WIDGETS'               => 'Single Ticker widget was not found',
	'FORM_INVALID'                 => 'Something wrong with form data',
	'ENTER_DATA'                   => 'Data was not set',
	'WIDGET_UPDATED'               => 'Single Ticker widget was updated',
	'WIDGET_ADDED'                 => 'Single Ticker widget was added',
	'WIDGET_REMOVED'               => 'Single Ticker widget was removed',
	'FORUM_TITLE'                  => 'Forum title',
	'TOPIC_TITLE'                  => 'Topic title',
	'SYMBOL_TITLE'                 => 'Symbol',
));